﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KnifeThrowerBehaviour : MonoBehaviour
{
    [SerializeField] GameObject _knifeGameObject;
    private bool _throwKnifePressed = false;

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            GameObject knifeGameObject = Instantiate(_knifeGameObject) as GameObject;
            knifeGameObject.transform.position = transform.position;
        }
    }
}
