﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class GameplayPanel : UIPanel
{
    [SerializeField] private Text _scoreLabel;
    [SerializeField] private Text _livesLabel;

    private void OnEnable()
    {
        Time.timeScale = 1.0f;
    }

    // Update is called once per frame
    void Update()
    {
        _scoreLabel.text = $"Knives: {LevelController.CurrentScore.ToString()}/{LevelController.ScoreRequired.ToString()}" ;
        _livesLabel.text = $"Lives: {LevelController.Lives.ToString()}";
    }
}
