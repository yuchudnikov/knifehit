﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KnifeBehaviour : MonoBehaviour
{
    [SerializeField] float _knifeSpeed = 5f;
    private bool _inAim = false;
    void Update()
    {
        if (!_inAim)
            transform.Translate(0, _knifeSpeed * Time.deltaTime, 0);
    }

    void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.tag == "Aim")
        {
            transform.SetParent(collider.transform);
            _inAim = true;
            LevelController.CurrentScore++;
        }

        if (collider.gameObject.tag == "Obstacle" ||
            collider.gameObject.tag == "Knife" &&
            !_inAim)
        {
            Destroy(gameObject);
            LevelController.Lives--;
        }
    }   
}
