﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    public UIPanelType CurrentPanelType { get; set; }
    private List<UIPanel> _panels = null;
    [SerializeField] LevelController _levelController;

    void Awake()
    {
        _panels = GetComponentsInChildren<UIPanel>(true).ToList();

        _levelController.EventPlayerLost += OnPlayerLost;
        _levelController.EventPlayerWins += OnPlayerWins;
    }

    private void OnDisable()
    {
        _levelController.EventPlayerLost -= OnPlayerLost;
        _levelController.EventPlayerWins -= OnPlayerWins;
    }

    void Start()
    {
        ShowPanel(UIPanelType.Gameplay);
    }

    public void ShowPanel(UIPanelType type)
    {
        foreach (var panel in _panels)
        {
            panel.gameObject.SetActive(panel.Type == type);
            if(panel.Type == type)
                CurrentPanelType = type;
        }
    }

    private void OnPlayerWins()
    {
        ShowPanel(UIPanelType.Win);
    }

    private void OnPlayerLost()
    {
        ShowPanel(UIPanelType.Gameover);
    }
}
