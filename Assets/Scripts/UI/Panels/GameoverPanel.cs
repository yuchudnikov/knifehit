﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameoverPanel : UIPanel
{
    private void OnEnable()
    {
        Time.timeScale = 0f;
    }
}
