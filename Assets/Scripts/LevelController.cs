﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelController : MonoBehaviour
{
    public Action EventPlayerWins;
    public Action EventPlayerLost;

    public static int CurrentScore = 0;
    public static int Lives = 2;
    [SerializeField] public static int ScoreRequired = 5;

    void Update()
    {
        if (CurrentScore >= ScoreRequired)
            EventPlayerWins?.Invoke();

        if (Lives <= 0)
            EventPlayerLost?.Invoke();
    }
}
