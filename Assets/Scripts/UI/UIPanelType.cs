﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum UIPanelType
{
    Undefined = 0,
    Gameplay = 1,
    Gameover = 2,
    Win = 3
}
