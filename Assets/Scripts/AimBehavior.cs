﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AimBehavior : MonoBehaviour
{
    [SerializeField] float _aimRotationSpeed = 10f;


    void Update()
    {
        transform.Rotate(0f, _aimRotationSpeed, 0f, Space.Self);
    }
}
